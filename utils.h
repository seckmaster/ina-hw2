#ifndef HW2_UTILS_H
#define HW2_UTILS_H

#include <set>
#include <boost/math/special_functions/zeta.hpp>

using namespace std;

template <class T>
function<T(T const &)> identity() {
    return [](auto val) { return val; };
}

template <class T>
function<bool(T const &, T const &)> desc() {
    return [](auto x, auto y) { return x > y; };
}

string exec(const char* cmd) {
    std::array<char, 128> buffer;
    std::string result;
    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
    if (!pipe) {
        throw std::runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
    }
    return result;
}

struct PlfitOutput {
    float gamma;
    float x_min;
    float L;
    float D;
    float p;
};

optional<PlfitOutput> parse_output(const string& output) {
    auto idx = output.find("C ");
    if (idx == string::npos) {
        idx = output.find("D ");
        if (idx == string::npos) {
            return {};
        }
    }
    auto substr  = output.substr(idx + 2);
    float gamma;
    float x_min;
    float L;
    float D;
    float p;
    sscanf(substr.c_str(), "%f %f %f %f %f", &gamma, &x_min, &L, &D, &p);
    return PlfitOutput { gamma, x_min, L, D, p };
}

template <typename T>
void write_to_file(const vector<T>& values, const string& file) {
    auto stream = ofstream(file);
    if (!stream.is_open()) { exit(99); }
    for (auto it = values.begin(); it != values.end(); it++) {
        stream << *it << endl;
    }
    stream.close();
}

template <typename T>
optional<PlfitOutput> power_law_fit(const vector<T>& values) {
    write_to_file(values, "../data/tmp.txt");
    auto output_text = exec("/usr/local/bin/plfit -b -p exact ../data/tmp.txt");
    remove("../data/tmp.txt");
    auto output = parse_output(output_text);
    return output;
}

// double zeta(double s, double t, int iter = 100) {
//     double acc = 0.0;
//     for (int n = 1; n <= iter; n++) {
//         double acc1 = 0.0;
//         for (int m = 1; m <= n; m++) {
//             acc1 += pow(m, t);
//         }
//         acc += (1.0 / pow(n + 1, s)) * acc1;
//     }
//     return acc;
// }

template<class T>
vector<double> power_law_distribution(const vector<T>& values, double gamma, int k_min) {
    vector<double> distr;
    distr.reserve(values.size());
    auto mul = 1.0 / boost::math::zeta(gamma);
    for (auto it = values.begin(); it != values.end(); it++) {
        auto k = static_cast<double>(*it);
        auto pk = mul * pow(k, -gamma);
        distr.push_back(pk);
    }
    return distr;
}

template<class T>
void print_best_nodes(const EmbellishedGraph<T> graph,
                      const vector<double>& centrality,
                      const string& name,
                      int n = 10) {
    cout << name << ":" << endl;
    auto indices = sort_with_indices<double>(centrality, desc<double>());
    for (int i = 0; i < n; i++) {
        cout << i + 1 << ": " << graph.get_embellishments()[indices[i]] << " [" << centrality[indices[i]] << "]"<< endl;
    }
}

vector<Graph::Node> largest_connected_component(const Graph& g) {
    std::vector<Graph::Node> *lcc = nullptr;
    auto components = g.get_weak_components();
    for (auto it = components.begin(); it != components.end(); it++) {
        if (lcc == nullptr || (lcc->size() < it->size())) {
            lcc = &*it;
        }
    }
    return *lcc;
}

void free_complist(igraph_vector_ptr_t *complist) {
    long int i;
    for (i = 0; i < igraph_vector_ptr_size(complist); i++) {
        igraph_destroy((igraph_t*) VECTOR(*complist)[i]);
        igraph_free(VECTOR(*complist)[i]);
    }
}

size_t largest_connected_component_size(const igraph_wrapper& g) {
    auto size = 0;
    auto components = g.connected_components_owned(IGRAPH_WEAK);
    for (int i = 0; i < igraph_vector_ptr_size(&components); i++) {
        auto component = (igraph_t*) VECTOR(components)[i];
        if (igraph_vcount(component) > size) {
            size = igraph_vcount(component);
        }
    }
    free_complist(&components);
    return size;
}

pair<vector<double>, vector<double>> probability_distribution(const std::map<Graph::Node, double>& degree_distr) {
    auto degree_sequence = std::vector<double>();
    auto probability_sequence = std::vector<double>();
    for (auto it = degree_distr.begin(); it != degree_distr.end(); it++) {
        degree_sequence.push_back(it->first);
        probability_sequence.push_back(it->second);
    }
    return make_pair(degree_sequence, probability_sequence);
}

Graph::Node highest_degree_node(const Graph& g) {
    Graph::Node result = 0;
    for (Graph::Node node = 1; node < g.get_nodes_count(); node++) {
        if (g.get_degree(result) < g.get_degree(node)) {
            result = node;
        }
    }
    return result;
}

set<Graph::Node> perform_random_walk(const Graph& g,
                                     float perc,
                                     int seed = 1322) {
    assert(perc >= 0.0);
    assert(perc <= 1.0);

    srand(seed);

    auto nodes_count = (size_t) ceil((float) g.get_nodes_count() * perc);
    set<Graph::Node> nodes;

    size_t current_node = rand() % g.get_nodes_count();
    while (nodes.size() < nodes_count) {
        nodes.insert(current_node);
        auto neighbours = g.get_neighbours(current_node);
        if (neighbours.empty()) {
            current_node = rand() % g.get_nodes_count();
            continue;
        }
        auto rand_edge = rand() % neighbours.size();
        current_node = neighbours[rand_edge];
    }
    return nodes;
}

pair<vector<double>, double> maximum_likelihood(const vector<int>& degrees,
                                                int k_min) {
    auto relevant_degrees = vector<double>();
    relevant_degrees.reserve(degrees.size());
    auto acc = 0.0;
    for (auto it = degrees.begin(); it != degrees.end(); it++) {
        auto degree = *it;
        if (degree < k_min) {
            continue;
        }
        acc += log((double) degree / ((double) k_min - 0.5));
        relevant_degrees.push_back(degree);
    }
    acc = 1.0f / acc;
    auto gamma = 1.0 + (double) relevant_degrees.size() * acc;
    return make_pair(relevant_degrees, gamma);
}

#endif //HW2_UTILS_H
