#include <EmbellishedGraph.h>
#include <Graph.h>
#include <GraphUtils.h>
#include <igraph_wrapper.h>

#include <functional>
#include <iostream>
#include <string>

#include "matplotlib-cpp/matplotlibcpp.h"
#include "utils.h"

using namespace std;

void exercise1() {
    auto graph = EmbellishedGraph<string>::read_edges_list_embellished(
        "../data/dolphins", 62, 159, false, identity<string>());
    print_graph_statistics(graph, "dolphins", true, true, true);

    auto SN100 =
        graph.find_node([](auto s) { return s.find("SN100") != string::npos; });
    cout << "SN100 degree: " << graph.get_degree(SN100) << endl << endl;

    auto betweenness = graph.get_betweenness_centrality();
    print_best_nodes(graph, betweenness, "Betweenness");
    cout << endl;
    auto closeness = graph.get_clustering_coefficient_centrality();
    print_best_nodes(graph, closeness, "Closeness");
    auto eigenvector = graph.get_eigenvector_centrality();
    print_best_nodes(graph, eigenvector, "Eigenvector");
    auto page_rank = graph.get_page_rank_centrality();
    print_best_nodes(graph, page_rank, "Page rank");
    auto hits = graph.get_hits_centrality();
    print_best_nodes(graph, hits.first, "HITS (Hubs)");
    print_best_nodes(graph, hits.second, "HITS (Auths)");

    {
        auto igraph = igraph_wrapper::read_graph_edgelist("../data/dolphins2", false);
        auto centrality = igraph.harmonic_mean_distance_non_owned();
        auto indices = sort_with_indices<double>(centrality, [](auto fst, auto snd) { return fst > snd; });
        cout << endl << endl;
        for (int i = 0; i < 10; i++) {
            cout << i + 1 << " [" << centrality[indices[i]] << "]" << graph.get_embellishment(indices[i]) << endl;
        }
        cout << endl << endl;
    }
    return;

    auto graph2 = graph.removing_node(SN100);
    print_graph_statistics(graph2, "dolphins wo/SN100");

    auto avg_distances = vector<double>();
    for (int i = 0; i < graph.get_nodes_count(); i++) {
        auto graph2 = graph.removing_node(i);
        avg_distances.push_back(graph2.get_avg_distance());
        cout << i << ": " << graph2.get_avg_distance() << ", "
             << graph2.get_diameter() << ", "
             << graph2.get_effective_diameter_bfs(40) << ", " << endl;
    }

    cout << endl;
    print_best_nodes(graph, avg_distances, "Avg. distances");
    cout << endl;

    auto betweenness_indices = sort_with_indices<double>(betweenness, desc<double>());
    for (int i = 0; i < 10; i++) {
        auto idx = betweenness_indices[i];
        auto graph2 = graph.removing_node(idx);
        cout << i << ": " << graph2.get_weak_components().size() << endl;
    }

    // TODO: - maybe consider link importance as well?
}

void exercise2() {
    namespace plt = matplotlibcpp;

    {
         auto graph = EmbellishedGraph<string>::read_edges_list_embellished(
             "../data/java", 2378, 14727, true, identity<string>());

         auto prob_distr_degree =
             probability_distribution(graph.get_degree_distribution());
         auto prob_distr_in_degree =
             probability_distribution(graph.get_in_degree_distribution());
         auto prob_distr_out_degree =
             probability_distribution(graph.get_out_degree_distribution());

         auto mle_degree = maximum_likelihood(graph.get_all_degrees(), 10);
         cout << "Gamma degree: " << mle_degree.second << endl;

         auto mle_in_degree = maximum_likelihood(graph.get_all_in_degrees(), 8);
         cout << "Gamma in-degree: " << mle_in_degree.second << endl;

         cout << "In-degree: " << graph.get_in_degree() << ", out-degree: " << graph.get_out_degree() << endl;

         plt::named_loglog("degree", prob_distr_degree.first,
                           prob_distr_degree.second, ".m");
         plt::named_loglog("in-degree", prob_distr_in_degree.first,
                           prob_distr_in_degree.second, ".b");
         plt::named_loglog("out-degree", prob_distr_out_degree.first,
                           prob_distr_out_degree.second, ".g");
         plt::legend();
         plt::show();
    }

    {
        auto graph = EmbellishedGraph<string>::read_edges_list_embellished(
            "../data/lucene", 2956, 10872, true, identity<string>());

        auto prob_distr_degree =
            probability_distribution(graph.get_degree_distribution());
        auto prob_distr_in_degree =
            probability_distribution(graph.get_in_degree_distribution());
        auto prob_distr_out_degree =
            probability_distribution(graph.get_out_degree_distribution());

        auto mle_degree = maximum_likelihood(graph.get_all_degrees(), 10);
        cout << "Gamma degree: " << mle_degree.second << endl;

        auto mle_in_degree = maximum_likelihood(graph.get_all_in_degrees(), 4);
        cout << "Gamma in-degree: " << mle_in_degree.second << endl;

        auto plf_degree = power_law_fit(graph.get_all_degrees());
        auto plf_in_degree = power_law_fit(graph.get_all_in_degrees());
        auto plf_out_degree = power_law_fit(graph.get_all_out_degrees());

        plt::named_loglog("degree", prob_distr_degree.first,
                          prob_distr_degree.second, ".m");
        plt::named_loglog("in-degree", prob_distr_in_degree.first,
                          prob_distr_in_degree.second, ".b");
        plt::named_loglog("out-degree", prob_distr_out_degree.first,
                          prob_distr_out_degree.second, ".g");
        plt::legend();
        plt::show();
    }
}

typedef vector<Graph::Node> ConnectedComponent;

vector<pair<ConnectedComponent, size_t>> attack_graph(Graph graph,
                                        bool random_or_highest_degree, // true: `random`, false: 'highest degree'
                                        int attack_count = 6,
                                        float increase_by = 0.1f,
                                        int seed = 555,
                                        bool debug_print = true) {
    srand(seed);
    
    auto connected_components = vector<pair<ConnectedComponent, size_t>>();
    connected_components.reserve(attack_count);
    
    for (int j = 0; j < attack_count; j++) {
        if (debug_print) cout << j << ": ";

        float percentage = j == 0 ? 0.0 : 0.1;
        size_t remove_nodes_count =
            (size_t)((float)graph.get_nodes_count()) * percentage;
        for (size_t i = 0; i < remove_nodes_count; i++) {
            auto node = random_or_highest_degree
                            ? rand() % graph.get_nodes_count()
                            : highest_degree_node(graph);
            graph.remove_node(node);
            if (debug_print && i % 1000 == 0) cout << i << ", ";
        }
        auto lcc = largest_connected_component(graph);
        connected_components.push_back(make_pair(lcc, graph.get_nodes_count()));

        if (debug_print)
            cout << lcc.size() << " ("
                 << ((float)lcc.size() / (float)graph.get_nodes_count()) << ")"
                 << endl;
    }
    return connected_components;
}

void exercise3(int r = 6) {
    namespace plt = matplotlibcpp;

    vector<float> percentages = { 0.0f, 0.1f, 0.2f, 0.3f, 0.4f, 0.5f };
    
    if (r == 0 || r == 4 || r == 6) {
        /**
        0.0: 75885 (1)
        0.1: 67327 (0.985797)
        0.2: 59347 (0.965494)
        0.3: 52173 (0.943079)
        0.4: 45512 (0.914079)
        0.5: 39644 (0.884694)
        */
        auto graph =
            Graph::read_edges_list("../data/nec", 75885, 357317, false).value();
        auto connected_components = attack_graph(graph, true);
        auto fractions = vector<float>();
        transform(connected_components.begin(), connected_components.end(),
                  back_insert_iterator(fractions), [&graph](auto el) {
                      return (float)el.first.size() / (float)el.second;
                  });
        plt::named_plot("random - nec", percentages, fractions, "-b");
    }
    if (r == 1 || r == 4 || r == 6) {
        /**
        0.0: 75885 (1)
        0.1: 44898 (0.657393)
        0.2: 527 (0.00857357)
        0.3: 18 (0.000325368)
        0.4: 10 (0.000200844)
        0.5: 2 (4.46319e-05)
        */
        auto graph =
            Graph::read_edges_list("../data/nec", 75885, 357317, false).value();
        auto connected_components = attack_graph(graph, false);
        auto fractions = vector<float>();
        transform(connected_components.begin(), connected_components.end(),
                  back_insert_iterator(fractions), [&graph](auto el) {
                      return (float)el.first.size() / (float)el.second;
                  });
        plt::named_plot("hubs - nec", percentages, fractions, "-g");
        plt::legend();
        plt::show();
    }
    if (r == 2 || r == 5 || r == 6) {
        /**
        0.0: 75878 (0.999908)
        0.1: 68284 (0.99981)
        0.2: 61443 (0.999593)
        0.3: 55272 (0.999096)
        0.4: 49704 (0.998273)
        0.5: 44645 (0.996296)
        */
        auto graph = Graph::generate_erdos_renyi(75885, 357317, false, 555);
        auto connected_components = attack_graph(graph, true);
        auto fractions = vector<float>();
        transform(connected_components.begin(), connected_components.end(),
                  back_insert_iterator(fractions), [&graph](auto el) {
                      return (float)el.first.size() / (float)el.second;
                  });
        plt::named_plot("random - erdos-renyi", percentages, fractions, "-b");
    }

    if (r == 3 || r == 5 || r == 6) {
        /**
        0.0: 75878 (0.999908)
        0.1: 68260 (0.999458)
        0.2: 61356 (0.998178)
        0.3: 55020 (0.994541)
        0.4: 49012 (0.984374)
        0.5: 42828 (0.955747)
        */
        auto graph = Graph::generate_erdos_renyi(75885, 357317, false, 555);
        auto connected_components = attack_graph(graph, false);
        auto fractions = vector<float>();
        transform(connected_components.begin(), connected_components.end(),
                  back_insert_iterator(fractions), [&graph](auto el) {
                      return (float)el.first.size() / (float)el.second;
                  });
        plt::named_plot("hubs - erdos-renyi", percentages, fractions, "-g");
        plt::legend();
        plt::show();
    }

    // make sure our implementation is correct ...
    //    auto graph = igraph_wrapper::read_graph_edgelist("../data/nec2",
    //    false); for (int j = 0; j <= 5; j++) {
    //        cout << j << ": ";
    //
    //        float percentage = j == 0 ? 0.0 : 0.1;
    //        size_t remove_nodes_count = (size_t) ((float)
    //        graph.vertices_count()) * percentage; for (size_t i = 0; i <
    //        remove_nodes_count; i++) {
    //            auto node = rand() % graph.vertices_count();
    //            graph.delete_vertex(node);
    //            if (i % 1000 == 0) { cout << i << ", "; }
    //        }
    //        auto lcc = largest_connected_component_size(graph);
    //        cout << lcc << " ("
    //             << ((float) lcc / (float) graph.vertices_count())
    //             << ")"
    //             << endl;
    //    }
}

void exercise4() {
    namespace plt = matplotlibcpp;

    auto graph =
        Graph::read_edges_list("../data/social", 10680, 24316, false).value();
        
    // {
    //     /**
    //     Nodes    | 10680 (0, 4229)
    //     Edges    | 24316
    //     <k>      | 4.55356 (205)
    //     Density  | 24313.7  <-- very dense??
    //     <C>      | 0.265945 <-- small world!
    //     Diameter | 24
    //     <d>      | 7.48554  <-- small world!
    //     LCC      | 100% (1)

    //     gamma    | 1.69967
    //     p        | 0.7616
    //     */
    //     print_graph_statistics(graph, "social");

    //     auto degree_distr_graph =
    //         probability_distribution(graph.get_degree_distribution());
    //     auto power_law = power_law_fit(degree_distr_graph.second).value();
    //     cout << "Gamma: " << power_law.gamma << ", p: " << power_law.p << endl
    //          << endl;

    //     plt::named_loglog("social", degree_distr_graph.first, degree_distr_graph.second, ".m");
    //     plt::legend();
    //     plt::show();
    // }

    {
        /**
        Nodes    | 1068 (0, 174)
        Edges    | 5672
        <k>      | 10.6217 (142)
        Density  | 5666.69
        <C>      | 0.474949
        Diameter | 15
        <d>      | 5.2614
        LCC      | 100% (1)

        gamma    | 1.69967
        p        | 0.7656
        */
        auto nodes = perform_random_walk(graph, 0.1);
        auto subgraph = graph.induce_subgraph(nodes);
        print_graph_statistics(subgraph, "random-walk social");

        auto degree_distr_subgraph =
            probability_distribution(subgraph.get_degree_distribution());

        auto power_law_subgraph =
            power_law_fit(subgraph.get_all_degrees()).value();
        cout << "Gamma: " << power_law_subgraph.gamma
             << ", p: " << power_law_subgraph.p << endl;

        plt::named_loglog("subgraph [social]", degree_distr_subgraph.first, degree_distr_subgraph.second, ".g");
        plt::legend();
        plt::show();
    }

    //    auto igraph = igraph_wrapper::read_graph_edgelist("../data/social2",
    //    false); cout << igraph.vertices_count() << endl
    //         << igraph.edges_count() << endl
    //         << igraph.average_degree() << endl
    //         << igraph.average_clustering_coefficient() << endl
    //         << igraph.diameter() << endl
    //         << igraph.avg_distances() << endl << endl;
    //
    //    auto subgraph2 = igraph.induced_subgraph(nodes);
    //    cout << subgraph2.vertices_count() << endl
    //         << subgraph2.edges_count() << endl
    //         << subgraph2.average_degree() << endl
    //         << subgraph2.average_clustering_coefficient() << endl
    //         << subgraph2.diameter() << endl
    //         << subgraph2.avg_distances() << endl;
}

void exercise5() {
    /**
    a. Better immunisation will provide the second scheme.
    b. This is because of "excess degree"; on average, the degree of a neighbour
    of any node is larger than the degree of that particular node. As we target
    nodes with highest degree possible (we are searching for hubs), it makes
    more sense to vaccinate the neighbours of randomly chosen nodes.
    */
}

// NOTE: - Don't forget to include Graph library
// into submission zip!
int main(int argc, char **argv) {
    if (argc > 1) {
        int exercise = atoi(argv[1]);
        switch (exercise) {
            case 1:
                exercise1();
                break;
            case 2:
                exercise2();
                break;
            case 3:
                if (argc > 2) {
                    exercise3(atoi(argv[2]));
                } else {
                    exercise3();
                }
                break;
            case 4:
                exercise4();
                break;
            case 5:
                exercise5();
                break;
            default:
                break;
        }
    } else {
        // exercise1();
        // exercise2();
        // exercise3(6);
        exercise4();
        // exercise5();
    }
    return 0;
}
